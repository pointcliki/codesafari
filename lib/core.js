// append element to tile at position x, y
appendElement = function(element, x, y) {
   $('tr:eq('+ y +') td:eq(' + x + ')').append(element);
};
appendObject = function( obj ) {
	appendElement(obj.html, obj.x, obj.y);
};

// get elements at position x, y
getElements = function(x, y) {
   return $('tr:eq('+ y +') td:eq(' + x + ')').children();
};

getLand = function(x, y) {
   return getElements(x, y)[0];
};

var Lion = function( x, y ) {
	this.x = x;
	this.y = y;
	this.html = $('<div class="lion"></div>');
   this.startX = x;
   this.startY = y;
};

var Duck = function( x, y ) {
	this.level = level;
   this.x = x;
   this.y = y;
   this.html = $('<div class="speech"></div><div class="duck"></div>');
   
   this.html.filter('.duck')[0].duck = this;
};

Duck.prototype = {

   showSpeech: function() {
      setTimeout(function() {
         $('.speech').fadeOut(1000);
      }, 3000);
      $('.speech').fadeIn(500);

   }

};

function right() { player.moveRight(1000); }
function left() { player.moveLeft(1000); }
function up() { player.moveUp(1000); }
function down() { player.moveDown(1000); }

Lion.prototype = {

	moveRight: function( i ) {
		i = i || 1;
		while (i--) {
			if (i > 0) showTrail(this.x, this.y, i);
			// Right arrow
			if (isValidMove(this.x, this.y, 'right')) {
				this.x += 1;
			} else {
				return;
			}
         // check if any actions can be performed at current position
         this.checkActionsAtPosition(this.x, this.y);
			// Update board
			appendObject(this);
		}
	},
	moveLeft: function( i ) {
		i = i || 1;
		while (i--) {
			if (i > 0) showTrail(this.x, this.y, i);
			// Left arrow
			if (isValidMove(this.x, this.y, 'left')) {
				this.x -= 1;
			} else {
				return;
			}
         // check if any actions can be performed at current position
         this.checkActionsAtPosition(this.x, this.y);
			// Update board
			appendObject(this);
		}
	},
	moveUp: function( i ) {
		i = i || 1;
		while (i--) {
			if (i > 0) showTrail(this.x, this.y, i);
			// Right arrow
			if (isValidMove(this.x, this.y, 'up')) {
				this.y -= 1;
			} else {
				return;
			}
         // check if any actions can be performed at current position
         this.checkActionsAtPosition(this.x, this.y);
			// Update board
			appendObject(this);
		}
	},
	moveDown: function( i ) {
		i = i || 1;
		while (i--) {
			if (i > 0) showTrail(this.x, this.y, i);
			// Left arrow
			if (isValidMove(this.x, this.y, 'down')) {
				this.y += 1;
			} else {
				return;
			}
         // check if any actions can be performed at current position
         this.checkActionsAtPosition(this.x, this.y);
			// Update board
			appendObject(this);
		}
	},
   checkActionsAtPosition: function() {
      playSound('move');
      // check for buttons, buttons are pressed if lionel finds one
		var b = $('tr:eq('+ this.y +') td:eq(' + this.x + ')').find(".button");
      if(b.length) {
         pressButton(b);
      } else {
         $('.button').removeClass('pbutton');
      }
      // check for gems, gems are collected if lionel finds them
      var g = $('tr:eq('+ this.y +') td:eq(' + this.x + ')').find(".gem:visible");
      if (g.length) {
         g.hide();
         gemsCollected++;
         playSound('gem');
      }
      // check neighbouring cells for other animals
      for (var r=0; r<3; r++) {
         for (var c=0; c<3; c++) {
            var idxtr = this.y-1+r;
            var idxtd = this.x-1+c;
            var duck = $('tr:eq('+ idxtr +') td:eq(' + idxtd + ')').find('.duck');
            if (duck.length && (r != 1 || c != 1)) {
               duck[0].duck.showSpeech();
            }
         }
      }
   },
   isDead: function() {
      var b = $('tr:eq('+ this.y +') td:eq(' + this.x + ')').find(".lowbridge");
      return b.length;
   }
};
showTrail = function( x, y, i ) {
    var lion = $('<div class="lion"></div>');
    lion.animate({
        opacity: 0
    }, i, function() {
        lion.remove();
    });
	appendElement(lion, x, y);
};
pressButton = function( b ) {
	b.addClass('pbutton');
	for (var i in b[0].bridges) b[0].bridges.eq(i).removeClass('lowbridge').addClass('bridge').css('background-position', '-240px -192px');
	setTimeout(function() {
		for (var i in b[0].bridges) b[0].bridges.eq(i).removeClass('bridge').addClass('lowbridge').css('background-position', '-240px -144px');
		
		// Check for death
      if (player.isDead()) {
     		$('.modal-body').html('<p>Lionel can\'t swim! Try again!</p>');
   		$('#modalInfo').modal('show');
         stopTimer();
      }
	}, b[0].time);
};

createIceCream = function() {
	return $('<div class="icecream"></div>');
};

createGem = function() {
   return $('<div class="gem"></div>');
};

createLand = function(type) {       
   return $('<div class="land ' + type + '"></div>');
};

createButton = function() {
	return $('<div class="button"></div>');
};

bindBridge = function(button, bridges, time) {
   button[0].bridges = bridges;
   button[0].time = time;
};

bindSpeech = function(duck, text, actions) {
   duck[0].text = text;
   duck.parent().find('.speech').html(text);
   duck[0].actions = actions;
}

isValidMove = function(x, y, direction) {

   // check position
   switch (direction) {
      
      case 'left':
         if (x <= 0) {
            return false;
         }
         x -= 1;
         break;
      case 'right':
         if (x >= 15) {
            return false;
         }
         x += 1;
         break;
      case 'up':
         if (y <= 0) {
            return false;
         }
         y -= 1;
         break;
      case 'down':
         if (y >= 15) {
            return false;
         }
         y += 1;
         break;
   }
   
   var nextTile = getLand(x, y, direction);
   
   if (getElements(x, y).eq(1).hasClass('icecream'))  {
      setTimeout(function() {
         completeLevel();
      }, 200);
	}

   var foundDuck = getElements(x, y).filter('.duck').length;
   return !($(nextTile).hasClass('water')) && !($(nextTile).hasClass('wall')) && !($(nextTile).hasClass('lowbridge')) && !(foundDuck);

};

$(function() {

	var KEY_TIMEOUT = 100;
	var keyTimestamp = new Date().getTime();

	$("body").keydown(function(event) {
		// Prevent moving with arrow keys
		if (event.keyCode >= 37 && event.keyCode <= 40) event.preventDefault();
		if (new Date().getTime() - keyTimestamp > 0) {
			switch (event.keyCode) {
			case 37:
				player.moveLeft();
				break;
			case 38:
				player.moveUp();
				break;
			case 39:
				player.moveRight();
				break;
			case 40:
				player.moveDown();
			}
		 keyTimestamp = new Date().getTime() + KEY_TIMEOUT;
		}
	});
});
