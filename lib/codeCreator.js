function addCode(input) {
    playSound('codeGo');
   $('#codeInput').animate({borderWidth: "25px"},"fast");
   $('#codeInput').animate({borderWidth: "5px"},"medium");
   updateTextBox();
   $('#codeInput').append($(input).text() + '\n');
   $('#codeInput').val($('#codeInput').html());
}

function updateTextBox() {
   $('#codeInput').html($('#codeInput').val());
}

function evaluateCode(){
    playSound('codeMove');
    try {
       eval($('#codeInput').val());       
       $('.codeError').hide();
       $('.enterButton').animate({borderWidth: "25px"},"fast");             
       $('.enterButton').animate({borderWidth: "0px"},"fast"); 
    } catch(e) {
       $('.codeError').show()
       $('.codeError').animate({opacity: 1},"fast");         
       $('.codeError').animate({borderWidth: "25px"},"fast");             
       $('.codeError').animate({borderWidth: "1px"},"fast");       
       console.log(e);
    }
}
