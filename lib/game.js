$(function() {
   resetLevel();

   $('.popupButton').click(function() {
      if (levelcompleted) {
         goToNextLevel();
      } else {
         resetLevel();
      }
   });
});

level = '1';
levelcompleted = true;

resetLevel = function() {

   stopTimer();
   clearLevel();
   console.log(level);
   switch (level){
    case '1':
        $('.modal-body').html('<p>Welcome to Code Safari</p>');
        $('#modalInfo').modal('show');
    break;
    case 2: 
        $('#modalInfo').modal('hide');
        $('.modal-body').html('<p>Welcome to Level 2</p>');
        $('#modalInfo').modal('show');
        console.log('On level 2');
        $('#modalInfo').modal('show');
    break;
   }

   $.get('levels/level' + level + '.tmx', function( data ) {
      var map = new TMXMap(data);
		var types = ['', '', '', '', '', '', '', '', '', '', '', '', '', '',
			'dirt', 'grass', 'water', 'ddirt', 'wall', 'diamond', 'icecream',
			'dirt', 'grass', 'water', 'ddirt', 'wall', 'lowbridge', 'lion',
			'dirt', 'grass', 'water', 'ddirt', 'wall', 'hibridge', '',
			'dirt', 'grass', 'water', 'ddirt', 'wall', 'pbutton', '',
			'dirt', 'grass', 'water', 'ddirt', 'wall', 'button', ''
		];
        for (var i in map.layers) {
            var layer = map.layers[i], data = layer.data;
			if (i == 0) {
				 for (var j in data) {
					var id = data[j];
					var tile = createLand(types[id]);
					tile.css('background-position', -((id % 7) * 48) + 'px ' + (-Math.floor(id / 7) * 48) + 'px');
					appendElement(tile, j % 16, Math.floor(j / 16));
				}
			} else {
				for (var j in data) {
					var id = data[j];
					if (id) {
						var obj;
						switch (id) {
							case 27:
								obj = player = new Lion(j % 16, Math.floor(j / 16));
								appendObject(obj);
								break;

							case 34:
								obj = new Duck(j % 16, Math.floor(j / 16));
                        appendObject(obj);
								break;

							case 20:
								obj = createIceCream();
								break;
								
							case 47:
								obj = createButton();
								break;
		
							case 19:
								obj = createGem();
								break;
							
						}
						obj.x = j % 16;
						obj.y = Math.floor(j / 16);
						appendElement(obj, obj.x, obj.y);
					}
				}
			}
           
        }
		for (var i in map.groups) {
			var group = map.groups[i];
			for (var j in group.objects) {
				var obj = group.objects[j];
				switch (obj.action) {
					case 'bridgebind':
						var b = getElements(obj.button.split(' ')[0], obj.button.split(' ')[1]).filter('.button');
						bindBridge(b, $('.lowbridge'), obj.time);
						break;
					
					case 'duck':
						var duck = getElements(obj.x / 48, obj.y / 48).find('.duck');
						bindSpeech(duck, obj.message, []);
						break;
				}
			}
		}
      if (levelcompleted) {
   		startTimer(60);
      } else {
         startTimer(); // resumes timer
      }
      levelcompleted = false;
    });
};

clearLevel = function() {
   $('td').empty();
   $('#codeInput').val('');
};

completeLevel = function() {
   levelcompleted = true;
   playSound('win');
   $('.modal-body').html('<p>You got the ice cream! Congratulations!</p>');
   $('#modalInfo').modal('show');
   stopTimer();
};

goToNextLevel = function() {
   level++;
   resetLevel();
};

failure = function() {
   levelcompleted = true;
   $('.modal-body').html('<p>Game Over: Try Again!</p>');
   $('#modalInfo').modal('show');
   level = 0; // incremented to 1 in goToNextLevel();
}


