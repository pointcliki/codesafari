(function() {
	var countDownInterval, time;
	
	var tick = function() {
		$('#time').text((time + '').length == 1 ? '0' + time : time);
		switch(time) {
			case 12:
				$('#timerImage').attr('src','img/icream2.png');
				break;
			case 10:
				$('#timerImage').attr('src','img/icream3.png');
				break;
			case 5:
				$('#timerImage').attr('src','img/icream4.png');  
				break;
			case 0:
				 $('#modalInfo').modal('show');
				break;
		}
		if (!time) {
			failure();
		    playSound('die');
			window.clearInterval(countDownInterval);
		}
		time--;
	};
	
	startTimer = function( t ) {
		if (t) time = t;
		countDownInterval = setInterval(tick, 1000);
		tick();
	};
	
	stopTimer = function() {
		if (countDownInterval) window.clearInterval(countDownInterval);
	};
})();
