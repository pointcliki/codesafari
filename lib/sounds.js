    var codeGo = new Audio('sounds/codeGo.wav');
    var dieSound = new Audio('sounds/die.wav');
    var codeMove = new Audio('sounds/codeMove.wav');
    var gemSound = new Audio('sounds/gem.wav');
    var moveSound =new Audio('sounds/move.wav');
    var winSound =new Audio('sounds/win.wav');
    var jumpingSound =new Audio('sounds/jumping.wav');
function playSound(soundType) {    
    //var snd = new Audio(source);
    switch (soundType){
        case "codeGo":
            codeGo.play();
        break;
        case "codeMove":
            codeMove.play();
        break;
        case "gem":
            gemSound.play();
        break;
        case "move":
            moveSound.play();
        break;
        case "win":
            winSound.play();
        break;
        case "die":
            dieSound.play();
        break;
        case "button":
            jumpingSound.play();
        break;
    }
}
